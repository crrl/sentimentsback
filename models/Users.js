const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let user = new Schema({
  name: String,
  email: String,
  password: {type:String, select:false}
});

const model = mongoose.model("users", user);

module.exports = model;