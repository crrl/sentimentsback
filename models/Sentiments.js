const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let sentiment = new Schema({
  userId: { type: Schema.Types.ObjectId, ref: 'user' },
  polarity: Number,
  type: String,
  fileName: String,
  date: Date
});

const model = mongoose.model("sentiments", sentiment);

module.exports = model;