'use strict';
const express = require('express');
const jwt    = require('jsonwebtoken');
const router = express.Router();
const {checkEmailDomain} = require('../constants/checkDomain');
const Users = require('../models/Users');


router.post('/api/authentication', async (req, res) => {
  let { email, password } = req.body.payload;
  const isValidEmail = checkEmailDomain(email);
  if (!isValidEmail) {
   return res.status(400).send({
      status: 400,
      message: 'El usuario no existe'
    });
  }

  const userFound = await Users.findOne({email, password}, '+password');
  if (!userFound) {
    return res.status(400).send({
      status: 400,
      message: 'El usuario no existe'
    });
  }

  let payload = {
    email,
    userId: userFound._id,

  }
  let token = jwt.sign(payload, '!*C0d3T3St*!', {
    expiresIn: 31536
  });
  res.status(200).send({
      token:token,
      email
  });
});

module.exports = router;
