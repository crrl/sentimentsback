'use strict';
const express = require('express');
const multer  = require('multer')
const fs = require('fs');
const path = require('path');
const axios = require('axios')

const Sentiments = require('../models/Sentiments');

const directoryPath = path.join(__dirname, '../uploads');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, req.authorization.userId +'-' + Date.now() + '-' + file.originalname) 
  }
})

const upload = multer({ storage,
  fileFilter: function (req, file, callback) {
    var ext = path.extname(file.originalname);
    if(ext !== '.txt' ) {
        return callback(new Error('Solo archivos txt son permitidos.'))
    }
    callback(null, true)
} })

const router = express.Router();


router.post('/api/upload',upload.array('file'),  (req, res) => {
  const date = new Date().toISOString().substring(0,10);
  for (let file of req.files) {
    fs.readFile(`${directoryPath}/${file.filename}`, 'latin1', async (err, data) => {
      if(err) {
        return res.status(500).send({errorMessage: 'Ha ocurrido un error.'});
      } else {
        const sentimApiResponse = await axios.post('https://sentim-api.herokuapp.com/api/v1/', {
          text: data
        })
        const sentimApiResult = sentimApiResponse.data.result 
        const newSentiment = {
          userId: req.userId,
          polarity: sentimApiResult.polarity,
          type: sentimApiResult.type,
          fileName: file.filename.split('-').splice(2).join('-'),
          date
        }
        await Sentiments.create(newSentiment);
      }
    });
  } 
  res.send('ok')
});

router.get('/api/files', async (req, res) => {
  const filesResponse = [];
  let sentiments = await Sentiments.find({userId: req.userId});
  fs.readdir(directoryPath, function (err, files) {
    if (err) {
        return res.status(404).send({errorMessage: 'Ocurrió un error al leer los archivos.'});
    } 
    files.forEach(function (file) {
      const fileOwner = file.split('-')[0];
      if (fileOwner === req.userId) {
        let fullName = file.split('-').splice(1).join('-');
        let fileName = file.split('-').pop();
        filesResponse.push({fullName, fileName})
      }
    });
    res.send({data: filesResponse, sentiments})
  });
});

router.post('/api/file/', (req, res) => {
  const fileFullName = `${req.userId}-${req.body.fileName}`
  fs.readFile(`${directoryPath}/${fileFullName}`, 'latin1', (err, data) => {
    if(err) {
      return res.status(500).send({errorMessage: 'Ha ocurrido un error.'});
    } else {
      res.send(data);
    }
  });
});

module.exports = router;
