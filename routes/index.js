const user = require('./authentication');
const files = require('./files');
const users = require('./users');

module.exports = {
    user,
    files,
    users
};