'use strict';
const express = require('express');
const Users = require('../models/Users');
const router = express.Router();
const { DEFAULT_ERROR_NO_DATA, DEFAULT_SUCCESS } =require('../constants/default-settings');


router.post('/api/user', async (req, res) => {
  let { email, name, password } = req.body;
  if (!email || !name || !password) {
    return res.status(404).send(DEFAULT_ERROR_NO_DATA);
  }

  const userFound = await Users.findOne({email});

  if (userFound) {
    return res.status(404).send({errorMessage: 'El usuario ya existe.'});
  }

  const newUser = {
    name,
    email,
    password
  }
  await Users.create(newUser);

  res.status(200).send(DEFAULT_SUCCESS);
});

module.exports = router;
