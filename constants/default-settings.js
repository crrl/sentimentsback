const DEFAULT_ERROR = {
  status:400, 
  response: 'La petición no puede ser procesada, intente más tarde.',
  errorMessage: null
};


const DEFAULT_ERROR_NO_DATA = {
  status:400, 
  response: 'Faltan datos.',
  errorMessage: null
};

const DEFAULT_SUCCESS = {
  status: 200, 
  response: 'Success'
};

const  config = { raw: true};


module.exports = {
  DEFAULT_ERROR,
  DEFAULT_SUCCESS,
  DEFAULT_ERROR_NO_DATA,
  config
}