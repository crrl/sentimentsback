const checkEmailDomain = (email) => {
  return(/@coppel.com\s*$/.test(email))
};

module.exports = {
  checkEmailDomain
};