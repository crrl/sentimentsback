'use strict';

const routes = require('./routes');
const cron = require('node-cron');
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const jwt    = require('jsonwebtoken');
const compression = require('compression')
const mongoose = require('mongoose');
require('dotenv').config()

const fs = require('fs')
const path = require('path');
const HOUR = 60;
const MINUTES = 60;
const HALF_DAY = 12;
const MILLISECONDS = 1000;
mongoose.connect(process.env.MONGO_URI)

let app = module.exports = express();

const filesPath = __dirname + '/uploads'


cron.schedule('* * * * *', () => {
  try {
    fs.readdir(filesPath, function(err, files) {
      files.forEach(function(file) {
        fs.stat(path.join(filesPath, file), function(err, stat) {
          var endTime, now;
          if (err) {
            return res.status(500).send({errorMessage: 'Ha ocurrido un error en el cron job.'});
          }
          now = new Date().getTime();
          endTime = new Date(stat.ctime).getTime() + MILLISECONDS * MINUTES  * HOUR * HALF_DAY;
          if (now > endTime && file.originalName !== '.gitkeep' ) {
            fs.unlinkSync(path.join(filesPath, file))
          }
        });
      });
    });
  } catch(err) {
    res.status(500).send({errorMessage: 'Ha ocurrido un error en el cron job.'});
  }
});

app.set('secretKey', '!*C0d3T3St*!');
app.use(compression())
app.use(cors({origin: process.env.FRONTEND_URL}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(function (req, res, next) {
  let isAuthPage = req.url.indexOf('/authentication');
  let isNewUser = req.url.indexOf('/user');
  if ( isAuthPage < 0 && isNewUser < 0) {
    jwt.verify(req.headers.authorization, app.get('secretKey'), function (err, decoded) {
      if (err) {
        res.status(500).send({'access': false, 'status':500});
      } else {
        req.authorization = decoded;
        req.userId = decoded.userId;
        next();
      }
    });
  } else {
    next();
  }
});


app.use(routes.user);
app.use(routes.files);
app.use(routes.users);
